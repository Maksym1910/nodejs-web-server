'use strict';

async function isExists(fs, path) {
  try {
    await fs.access(path);
    return true;
  } catch (error) {
    return false;
  }
}

function isValidFileFormat(file) {
  const regex = /[a-zA-Z0-9]*\.(log|txt|json|yaml|xml|js)$/g;
  return regex.test(file);
}

module.exports = { isExists, isValidFileFormat };
