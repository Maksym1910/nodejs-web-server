'use strict';

const express = require('express');
const morgan = require('morgan');
const fs = require('fs').promises;
const { isExists, isValidFileFormat } = require('./../utils');
const app = express();
const PORT = 8080;

app.use(express.json());
app.use(morgan('tiny'));

app.post('/api/files', (request, response) => {
  const requestedFilename = request.body.filename;
  const requestedContent = request.body.content;
  (async () => {
    try {
      if (!isValidFileFormat(requestedFilename)) {
        throw new Error('invalid file format');
      }
      if (requestedContent === undefined) {
        throw new Error('undefiend content parameter');
      }

      await fs.writeFile(`./api/files/${requestedFilename}`, `${requestedContent}`, 'utf-8');
      response.status(200).json({
        message: 'File created successfully',
      });
    } catch (error) {
      if (error.message === 'undefiend content parameter') {
        response.status(400).json({
          message: 'Please specify \'content\' parameter',
        });
      } else if (error.message === 'invalid file format') {
        response.status(400).json({
          message: 'Invalid file format. Supported formats: log|txt|json|yaml|xml|js',
        });
      } else {
        response.status(500).json({
          message: 'Server error',
        });
      }
    }
  })();
});

app.get('/api/files', (request, response) => {
  (async () => {
    try {
      if (!await isExists(fs, './api/files')) {
        throw new Error('Directory not founded');
      }
      const files = await fs.readdir('./api/files');
      response.status(200).json({
        message: 'Success',
        files,
      });
    } catch (error) {
      if (error.message === 'Directory not founded') {
        response.status(400).json({
          message: 'Client error',
        });
      } else {
        response.status(500).json({
          message: 'Server error',
        });
      }
    }
  })();
});

app.get('/api/files/:filename', (request, response) => {
  (async () => {
    const requestedFilename = request.params.filename;
    const existedFiles = await fs.readdir('./api/files');

    try {
      if (!existedFiles.some(filename => filename === requestedFilename)) {
        throw new Error('no such file');
      }

      const content = await fs.readFile(`./api/files/${requestedFilename}`);
      const fileExtension = requestedFilename.split('.').pop();
      const uploadedDate = (await fs.stat(`./api/files/${requestedFilename}`)).birthtime;

      response.status(200).json({
        message: 'Success',
        filename: requestedFilename,
        content: content.toString(),
        extension: fileExtension,
        uploadedDate,
      });
    } catch (error) {
      if (error.message === 'no such file') {
        response.status(400).json({
          message: `No file with ${requestedFilename} filename found`,
        });
      } else {
        response.status(500).json({
          message: 'Server error',
        });
      }
    }
  })();
});

app.put('/api/files/modify/:filename', (request, response) => {
  (async () => {
    try {
      if (!await isExists(fs, `./api/files/${request.params.filename}`)) {
        throw new Error('File not founded');
      }
      await fs.writeFile(`./api/files/${request.params.filename}`, `${request.body.content}`, 'utf-8');
      response.status(200).json({
        message: 'File modified successfully',
      });
    } catch (error) {
      if (error.message === 'File not founded') {
        response.status(400).json({
          message: error.message,
        });
      } else {
        response.status(500).json({
          message: 'Server error',
        });
      }
    }
  })();
});

app.delete('/api/files/delete/:filename', (request, response) => {
  (async () => {
    try {
      if (!await isExists(fs, `./api/files/${request.params.filename}`)) {
        throw new Error('File not founded');
      }
      await fs.unlink(`./api/files/${request.params.filename}`);
      response.status(200).json({
        message: 'File deleted successfully',
      });
    } catch (error) {
      if (error.message === 'File not founded') {
        response.status(400).json({
          message: error.message,
        });
      } else {
        response.status(500).json({
          message: 'Server error',
        });
      }
    }
  })();
});

app.listen(PORT);
